{
  "data_set_description": {
    "collection_date": "2020-07-22",
    "contributor": [
      "Allison Marie Horst",
      "Alison Presmanes Hill",
      "Kristen B Gorman",
      "Palmer Station Antarctica LTER"
    ],
    "creator": [
      "Allison Marie Horst",
      "Alison Presmanes Hill",
      "Kristen B Gorman",
      "Palmer Station Antarctica LTER"
    ],
    "default_target_attribute": "species",
    "description": "![palmerpenguins](https://github.com/allisonhorst/palmerpenguins/raw/master/man/figures/logo.png)\n\n## Description\n\nThe goal of palmerpenguins is to provide a great dataset for data exploration & visualization, as an alternative to iris.\n\nData were collected and made available by Dr. Kristen Gorman and the Palmer Station, Antarctica LTER, a member of the Long Term Ecological Research Network.\n\nPlease see [https://github.com/allisonhorst/palmerpenguins](https://github.com/allisonhorst/palmerpenguins) for more information.\n\n## Citation\nAnyone interested in publishing the data should contact [Dr. Kristen Gorman](https://www.uaf.edu/cfos/people/faculty/detail/kristen-gorman.php) about analysis and working together on any final products. From Gorman et al. (2014): _\"Individuals interested in using these data are expected to follow the US LTER Network’s Data Access Policy, Requirements and Use Agreement: https://lternet.edu/data-access-policy/.\"_\n\nThis dataset has been derived from the R package palmerpenguins available from [https://allisonhorst.github.io/palmerpenguins/](https://allisonhorst.github.io/palmerpenguins/).\n\nPlease cite as follows in publications:\n\n    Horst AM, Hill AP, Gorman KB (2020). palmerpenguins: Palmer\n    Archipelago (Antarctica) penguin data. R package version 0.1.0.\n    https://allisonhorst.github.io/palmerpenguins/\n\nA BibTeX entry for LaTeX users is:\n\n    @Manual{,\n        title = {palmerpenguins: Palmer Archipelago (Antarctica) penguin data},\n        author = {Allison Marie Horst and Alison Presmanes Hill and Kristen B Gorman},\n        year = {2020},\n        note = {R package version 0.1.0},\n        url = {https://allisonhorst.github.io/palmerpenguins/},\n    }\n\n\n## Artwork\n\nYou can download palmerpenguins art (useful for teaching with the data) from the Github repo or the R package. If you use this artwork, please cite with: \"Artwork by @allison_horst\".\n\n#### Meet the Palmer penguins\n\n<img src=\"https://github.com/allisonhorst/palmerpenguins/raw/master/man/figures/lter_penguins.png\" data-canonical-src=\"https://github.com/allisonhorst/palmerpenguins/raw/master/man/figures/lter_penguins.png\" width=\"500\">\n\n#### Bill dimensions\n\nThe culmen is the upper ridge of a bird’s bill. In this simplified dataset, culmen length and depth are renamed as variables bill_length_mm and bill_depth_mm to be more intuitive.\n\nFor this penguin data, the culmen (bill) length and depth are measured as shown below (thanks Kristen Gorman for clarifying!):\n\n<img src=\"https://github.com/allisonhorst/palmerpenguins/raw/master/man/figures/culmen_depth.png\" data-canonical-src=\"https://github.com/allisonhorst/palmerpenguins/raw/master/man/figures/culmen_depth.png\" width=\"500\">",
    "description_version": "1",
    "file_id": "21854866",
    "format": "ARFF",
    "id": "42585",
    "licence": "CC0",
    "md5_checksum": "0d4936a104bb85ef5127a5a63dce81f9",
    "minio_url": "https://data.openml.org/datasets/0004/42585/dataset_42585.pq",
    "name": "penguins",
    "original_data_url": "https://github.com/allisonhorst/palmerpenguins",
    "paper_url": "https://doi.org/10.1371/journal.pone.0090081",
    "parquet_url": "https://data.openml.org/datasets/0004/42585/dataset_42585.pq",
    "processing_date": "2020-07-22 18:35:02",
    "status": "active",
    "tag": "Kaggle",
    "upload_date": "2020-07-22T18:34:52",
    "url": "https://api.openml.org/data/v1/download/21854866/penguins.arff",
    "version": "1",
    "visibility": "public"
  }
}