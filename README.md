# OpenML dataset: penguins

https://www.openml.org/d/42585

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

![palmerpenguins](https://github.com/allisonhorst/palmerpenguins/raw/master/man/figures/logo.png)

## Description

The goal of palmerpenguins is to provide a great dataset for data exploration & visualization, as an alternative to iris.

Data were collected and made available by Dr. Kristen Gorman and the Palmer Station, Antarctica LTER, a member of the Long Term Ecological Research Network.

Please see [https://github.com/allisonhorst/palmerpenguins](https://github.com/allisonhorst/palmerpenguins) for more information.

## Citation
Anyone interested in publishing the data should contact [Dr. Kristen Gorman](https://www.uaf.edu/cfos/people/faculty/detail/kristen-gorman.php) about analysis and working together on any final products. From Gorman et al. (2014): _"Individuals interested in using these data are expected to follow the US LTER Network’s Data Access Policy, Requirements and Use Agreement: https://lternet.edu/data-access-policy/."_

This dataset has been derived from the R package palmerpenguins available from [https://allisonhorst.github.io/palmerpenguins/](https://allisonhorst.github.io/palmerpenguins/).

Please cite as follows in publications:

    Horst AM, Hill AP, Gorman KB (2020). palmerpenguins: Palmer
    Archipelago (Antarctica) penguin data. R package version 0.1.0.
    https://allisonhorst.github.io/palmerpenguins/

A BibTeX entry for LaTeX users is:

    @Manual{,
        title = {palmerpenguins: Palmer Archipelago (Antarctica) penguin data},
        author = {Allison Marie Horst and Alison Presmanes Hill and Kristen B Gorman},
        year = {2020},
        note = {R package version 0.1.0},
        url = {https://allisonhorst.github.io/palmerpenguins/},
    }


## Artwork

You can download palmerpenguins art (useful for teaching with the data) from the Github repo or the R package. If you use this artwork, please cite with: "Artwork by @allison_horst".

#### Meet the Palmer penguins

<img src="https://github.com/allisonhorst/palmerpenguins/raw/master/man/figures/lter_penguins.png" data-canonical-src="https://github.com/allisonhorst/palmerpenguins/raw/master/man/figures/lter_penguins.png" width="500">

#### Bill dimensions

The culmen is the upper ridge of a bird’s bill. In this simplified dataset, culmen length and depth are renamed as variables bill_length_mm and bill_depth_mm to be more intuitive.

For this penguin data, the culmen (bill) length and depth are measured as shown below (thanks Kristen Gorman for clarifying!):

<img src="https://github.com/allisonhorst/palmerpenguins/raw/master/man/figures/culmen_depth.png" data-canonical-src="https://github.com/allisonhorst/palmerpenguins/raw/master/man/figures/culmen_depth.png" width="500">

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42585) of an [OpenML dataset](https://www.openml.org/d/42585). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42585/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42585/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42585/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

